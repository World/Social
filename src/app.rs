//
// app.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gio::prelude::*;
use gio::subclass::prelude::*;
use glib::translate::*;
use gtk::prelude::*;

use elefren::{http_send::HttpSender, registration::Registered, Mastodon};

use std::cell::RefCell;

use crate::i18n::*;
use crate::{config, passwords, utils, window::SoclWindow};

pub struct SoclApplicationPrivate {
    pub window: RefCell<Option<SoclWindow>>,
    pub registration: RefCell<Option<Registered<HttpSender>>>,
}

impl ObjectSubclass for SoclApplicationPrivate {
    const NAME: &'static str = "SoclApplication";
    type ParentType = gtk::Application;
    type Instance = glib::subclass::simple::InstanceStruct<Self>;
    type Class = glib::subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        Self {
            window: RefCell::new(None),
            registration: RefCell::new(None),
        }
    }
}

impl ObjectImpl for SoclApplicationPrivate {}

impl gio::subclass::prelude::ApplicationImpl for SoclApplicationPrivate {
    fn startup(&self, application: &gio::Application) {
        info!("SoclApplication::startup");
        self.parent_startup(application);

        libhandy::functions::init();

        let gtk_settings = gtk::Settings::get_default().expect("Could not get gtk::Settings");
        check_theme(&gtk_settings);
        gtk_settings.connect_property_gtk_theme_name_notify(check_theme);
        gtk_settings.connect_property_gtk_icon_theme_name_notify(check_theme);
        info!("{} Version {}", config::APP_ID, config::VERSION);
    }

    fn activate(&self, app: &gio::Application) {
        if let Some(ref window) = *self.window.borrow() {
            window.present();
            return;
        }

        let app = app.downcast_ref::<SoclApplication>().unwrap();
        let window = SoclWindow::new(&app);
        window.present();
        self.window.replace(Some(window));
        app.setup_gactions();
        glib::timeout_add_local(
            std::time::Duration::from_millis(25),
            clone!(@strong app => move || app.setup_channel()),
        );
    }

    fn open(&self, application: &gio::Application, files: &[gio::File], _hint: &str) {
        application.activate();

        if self
            .window
            .borrow()
            .as_ref()
            .unwrap()
            .main_stack
            .get_visible_child_name()
            .unwrap()
            == "register"
            && files[0].has_uri_scheme("oauth2redirect")
        {
            let mut uri = files[0].get_uri().to_string();
            uri = uri.trim_end_matches("&state=").to_string();
            let _uri = uri
                .trim_start_matches(&format!("oauth2redirect://{}/?code=", config::APP_ID))
                .to_string();

            if let Some(reg) = self.registration.borrow().as_ref() {
                let masto = reg.complete(&_uri).unwrap();
                passwords::store_data(&masto.data);
                let sender = self.window.borrow().as_ref().unwrap().sender.clone();
                send!(sender, Actions::Login(masto.clone()));
            };
        }
    }
}

impl gtk::subclass::application::GtkApplicationImpl for SoclApplicationPrivate {}

glib_wrapper! {
    pub struct SoclApplication(Object<glib::subclass::simple::InstanceStruct<SoclApplicationPrivate>, glib::subclass::simple::ClassStruct<SoclApplicationPrivate>, SoclApplicationClass>) @extends gio::Application, gtk::Application, @implements gio::ActionGroup, gio::ActionMap;

    match fn {
        get_type => || SoclApplicationPrivate::get_type().to_glib(),
    }
}

#[derive(Clone, Debug)]
pub enum Actions {
    Login(Mastodon),
    ShowRegisterPage,
}

impl SoclApplication {
    pub fn new() -> Self {
        let application = glib::Object::new(
            SoclApplication::static_type(),
            &[
                ("application-id", &Some(config::APP_ID)),
                ("flags", &gio::ApplicationFlags::HANDLES_OPEN),
            ],
        )
        .unwrap()
        .downcast::<SoclApplication>()
        .unwrap();

        application.set_resource_base_path(Some("/org/gnome/Social"));
        application
    }

    pub fn setup_gactions(&self) {
        action!(
            self,
            "quit",
            clone!(@weak self as app => move |_, _| {
                app.quit();
            })
        );
        self.set_accels_for_action("app.quit", &["<Primary>q"]);

        action!(
            self,
            "logout",
            clone!(@weak self as app => move |_, _| {
                passwords::delete_passwords();
                let sender = app.get_window().borrow().as_ref().unwrap().sender.clone();
                sender.send(Actions::ShowRegisterPage).unwrap();
            })
        );

        action!(
            self,
            "about",
            clone!(@weak self as app => move |_, _| {
                app.show_about();
            })
        );

        let v = glib::VariantTy::new("s").unwrap();
        action!(
            self,
            "error",
            Some(v),
            clone!(@weak self as app => move |_, v| {
                let window = app.get_active_window().unwrap();
                let variant = v.unwrap();
                let msg = variant.get::<String>().unwrap();
                utils::error_dialog(&msg, &window);
            })
        );

        action!(
            self,
            "gab-redirect",
            clone!(@weak self as app => move |_, _| {
                if let Some(win) = app.get_active_window() {
                    let _ = gtk::show_uri(Some(&win), "https://medium.com/@juliaserano/free-speech-and-the-paradox-of-tolerance-e0547aefe538", 0);
                    let _ = gtk::show_uri(Some(&win), "https://www.lifeafterhate.org/", 0);
                }
            })
        );
    }

    pub fn setup_channel(&self) -> glib::Continue {
        if let Ok(action) = self
            .get_window()
            .borrow()
            .as_ref()
            .unwrap()
            .receiver
            .try_recv()
        {
            match action {
                Actions::ShowRegisterPage => {
                    let w = self.get_window().borrow();
                    let win = w.as_ref().unwrap();

                    win.main_stack.set_visible_child_name("register");
                }
                Actions::Login(client) => {
                    let w = self.get_window().borrow();
                    let win = w.as_ref().unwrap();

                    let mut client_writer = win.client.write().expect("Could not write to client");
                    *client_writer = Some(client);
                    drop(client_writer);
                    win.setup_main_content();
                }
            }
        };

        glib::Continue(true)
    }

    pub fn get_default() -> Option<SoclApplication> {
        let app = gio::Application::get_default()?.downcast().unwrap();
        Some(app)
    }

    pub fn get_window(&self) -> &RefCell<Option<SoclWindow>> {
        &SoclApplicationPrivate::from_instance(self).window
    }

    pub fn get_registration(&self) -> &RefCell<Option<Registered<HttpSender>>> {
        &SoclApplicationPrivate::from_instance(self).registration
    }

    pub fn show_about(&self) {
        let window = self.get_active_window();
        let artists = vec!["Tobias Bernard"];
        let authors = vec!["Christopher Davis <christopherdavis@gnome.org>"];

        let dialog = gtk::AboutDialog::new();
        dialog.set_artists(&artists);
        dialog.set_authors(&authors);
        dialog.set_comments(Some(&i18n("A federated microblogging client for GNOME")));
        dialog.set_translator_credits(Some(&i18n("translator-credits")));
        dialog.set_license_type(gtk::License::Gpl30);
        dialog.set_logo_icon_name(Some(config::APP_ID));
        dialog.set_wrap_license(true);
        dialog.set_version(Some(config::VERSION));
        dialog.set_copyright(Some(
            format!("\u{A9} {} Christopher Davis, et al.", config::COPYRIGHT).as_str(),
        ));

        if let Some(w) = window {
            dialog.set_transient_for(Some(&w));
            dialog.set_modal(true);
        }

        dialog.present();
    }

    pub fn run() {
        gettextrs::setlocale(gettextrs::LocaleCategory::LcAll, "");
        gettextrs::bindtextdomain("gnome-social", config::LOCALEDIR);
        gettextrs::textdomain("gnome-social");

        let application = SoclApplication::new();

        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/org/gnome/Social/stylesheet.css");
        gtk::StyleContext::add_provider_for_display(
            &gdk::Display::get_default().unwrap(),
            &provider,
            600,
        );

        glib::set_application_name(&format!("Social{}", config::NAME_SUFFIX));
        glib::set_program_name(Some("gnome-social"));
        gtk::Window::set_default_icon_name(config::APP_ID);
        let args: Vec<String> = std::env::args().collect();
        application.run(&args.as_slice());
    }
}

// Code from https://gitlab.gnome.org/World/podcasts/-/merge_requests/168/
// (C) Jordan Petridis 2020
fn check_theme(settings: &gtk::Settings) {
    let theme_name = settings
        .get_property_gtk_theme_name()
        .map(|s| s.to_string())
        .unwrap();
    let icon_theme_name = settings
        .get_property_gtk_icon_theme_name()
        .map(|s| s.to_string())
        .unwrap();

    if theme_name != "Adwaita"
        && theme_name != "HighContrast"
        && theme_name != "HighContrastInverse"
    {
        settings.set_property_gtk_theme_name(Some("Adwaita"));
    }

    if icon_theme_name != "Adwaita" {
        settings.set_property_gtk_icon_theme_name(Some("Adwaita"));
    }
}
