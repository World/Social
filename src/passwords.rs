//
// passwords.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use elefren::Data;
use secret_service::{EncryptionType, SecretService};

static SECRET_SERVICE_KEY: &str = "GNOME Social Credentials";

pub fn store_data(data: &Data) {
    let key = SECRET_SERVICE_KEY;
    let ss = SecretService::new(EncryptionType::Dh)
        .expect("Could not start secret-service D-bus session");
    let collection = ss
        .get_default_collection()
        .expect("Could not get default collection from secret service");

    let d = serde_json::to_string(&data).unwrap();
    collection
        .create_item(
            key,
            vec![("base", &String::from(data.clone().base))],
            d.as_bytes(),
            true,
            "application/json",
        )
        .expect("Could not write secret to collection");
}

pub fn delete_passwords() {
    let key = SECRET_SERVICE_KEY;
    let ss = SecretService::new(EncryptionType::Dh)
        .expect("Could not start secret-service D-bus session");
    let collection = ss
        .get_default_collection()
        .expect("Could not get default collection from secret service");
    let pwds = collection
        .get_all_items()
        .expect("Could not get items from default collection");

    let pwd = pwds
        .iter()
        .find(|x| x.get_label().unwrap_or_default() == key);

    let d = pwd.unwrap();
    d.unlock().unwrap();
    d.delete().unwrap();
}

pub fn get_data() -> Option<Data> {
    let key = SECRET_SERVICE_KEY;
    let ss = SecretService::new(EncryptionType::Dh)
        .expect("Could not start secret-service D-bus session");
    let collection = ss
        .get_default_collection()
        .expect("Could not get default collection from secret service");
    let pwds = collection
        .get_all_items()
        .expect("Could not get items from default collection");

    let pwd = pwds
        .iter()
        .find(|x| x.get_label().unwrap_or_default() == key)?;

    pwd.unlock().unwrap();
    let secret = pwd.get_secret().unwrap();
    let secret_str: String = String::from_utf8(secret).unwrap();
    let data: Data = serde_json::from_str(&secret_str).unwrap();
    Some(data)
}
