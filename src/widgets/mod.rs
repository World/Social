//
// mod.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod notification_widget;
mod pages;
mod post_builder;
mod timelines;
mod toot_widget;

// Export for use as widgets::*
pub use self::notification_widget::NotificationWidget;
pub use self::pages::MainContentPage;
pub use self::pages::NotificationsPage;
pub use self::pages::RegisterPage;
pub use self::post_builder::PostBuilder;
pub use self::toot_widget::TootWidget;
