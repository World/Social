// register_page.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gio::prelude::*;
use glib::translate::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use std::thread;

use crate::i18n::*;
use crate::{app::SoclApplication, utils};

#[derive(Clone, Debug)]
pub struct RegisterPagePriv {
    headerbar: libhandy::HeaderBar,
    clamp: libhandy::Clamp,
    instance_entry: gtk::Entry,
    authorize_button: gtk::Button,
}

impl ObjectSubclass for RegisterPagePriv {
    const NAME: &'static str = "SoclRegisterPage";
    type ParentType = gtk::Box;
    type Instance = glib::subclass::simple::InstanceStruct<Self>;
    type Class = glib::subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        let builder = gtk::Builder::from_resource("/org/gnome/Social/gtk/register_page.ui");

        let headerbar = builder.get_object("register_header").unwrap();
        let clamp = builder.get_object("register_page").unwrap();
        let instance_entry = builder.get_object("instance_entry").unwrap();
        let authorize_button = builder.get_object("authorize_button").unwrap();

        Self {
            headerbar,
            clamp,
            instance_entry,
            authorize_button,
        }
    }
}

impl ObjectImpl for RegisterPagePriv {
    fn constructed(&self, obj: &glib::Object) {
        let self_ = obj.downcast_ref::<RegisterPage>().unwrap();
        self_.append(&self.headerbar);
        self_.append(&self.clamp);

        self.instance_entry.connect_activate(entry_activated_cb);
        self.authorize_button.connect_clicked(
            clone!(@weak self.instance_entry as entry => move |_| {
                entry_activated_cb(&entry);
            }),
        );
    }
}

impl WidgetImpl for RegisterPagePriv {}
impl BoxImpl for RegisterPagePriv {}

glib_wrapper! {
    pub struct RegisterPage(Object<glib::subclass::simple::InstanceStruct<RegisterPagePriv>,
        glib::subclass::simple::ClassStruct<RegisterPagePriv>,
        RegisterPageClass>) @extends gtk::Widget, gtk::Box;

    match fn {
        get_type => || RegisterPagePriv::get_type().to_glib(),
    }
}

impl RegisterPage {
    pub fn new() -> Self {
        let page = glib::Object::new(
            RegisterPage::static_type(),
            &[("orientation", &gtk::Orientation::Vertical)],
        )
        .unwrap()
        .downcast::<RegisterPage>()
        .unwrap();

        page.show();
        page
    }
}

fn entry_activated_cb(entry: &gtk::Entry) {
    let app = gio::Application::get_default()
        .unwrap()
        .downcast::<SoclApplication>()
        .unwrap();

    if let Some(entry_text) = entry.get_text() {
        let mut text = entry_text.to_string();
        // Do not facilitate the platforming of white supremacy.
        if !text.contains("gab.com") && !text.contains("gab.ai") {
            let auth_thread = thread::spawn(move || {
                if !text.starts_with("https://") && !text.starts_with("http://") {
                    // Default to HTTPS, but allow http connections
                    // for testing purposes.
                    text = format!("https://{}", text);
                }
                utils::authorize_url(&text).expect("Failed To Authorize URL")
            });

            match auth_thread.join() {
                Ok(reg) => {
                    app.get_registration().replace(Some(reg));
                }
                Err(_) => {
                    app.lookup_action("error")
                        .unwrap()
                        .downcast::<gio::SimpleAction>()
                        .unwrap()
                        .activate(Some(&i18n("Failed to authorize URL").to_variant()));
                }
            };
        } else {
            app.lookup_action("gab-redirect")
                .unwrap()
                .downcast::<gio::SimpleAction>()
                .unwrap()
                .activate(None);
        }
    }
}
