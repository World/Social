// main_content_page.rs
//
// Copyright 2020 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::translate::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use std::rc::Rc;

use once_cell::sync::OnceCell;

use crate::widgets::timelines;
use crate::ClientRef;

#[derive(Clone, Debug)]
pub struct MainContentPagePriv {
    headerbar: libhandy::HeaderBar,
    account_button: gtk::MenuButton,
    account_popover: gtk::PopoverMenu,
    switcher_title: libhandy::ViewSwitcherTitle,
    notifs_button: gtk::Button,
    menu_button: gtk::MenuButton,
    primary_menu: gio::MenuModel,
    stack: gtk::Stack,
    home_page: gtk::ScrolledWindow,
    switcher_bar: libhandy::ViewSwitcherBar,
    client: OnceCell<ClientRef>,
    home_timeline: OnceCell<Rc<timelines::Home>>,
}

impl ObjectSubclass for MainContentPagePriv {
    const NAME: &'static str = "SoclMainContentPage";
    type ParentType = gtk::Box;
    type Instance = glib::subclass::simple::InstanceStruct<Self>;
    type Class = glib::subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        let builder = gtk::Builder::from_resource("/org/gnome/Social/gtk/main_content_page.ui");
        builder
            .add_from_resource("/org/gnome/Social/gtk/primary_menu.ui")
            .unwrap();
        builder
            .add_from_resource("/org/gnome/Social/gtk/account_popover.ui")
            .unwrap();

        Self {
            headerbar: get_widget!(builder, libhandy::HeaderBar, @main_header),
            account_button: get_widget!(builder, gtk::MenuButton, @account_button),
            account_popover: get_widget!(builder, gtk::PopoverMenu, @account_popover),
            switcher_title: get_widget!(builder, libhandy::ViewSwitcherTitle, @switcher_title),
            notifs_button: get_widget!(builder, gtk::Button, @notifs_button),
            menu_button: get_widget!(builder, gtk::MenuButton, @menu_button),
            primary_menu: get_widget!(builder, gio::MenuModel, @primary_menu),
            stack: get_widget!(builder, gtk::Stack, @timelines_stack),
            home_page: get_widget!(builder, gtk::ScrolledWindow, @home_page),
            switcher_bar: get_widget!(builder, libhandy::ViewSwitcherBar, @switcher_bar),
            client: OnceCell::new(),
            home_timeline: OnceCell::new(),
        }
    }
}

impl ObjectImpl for MainContentPagePriv {
    fn constructed(&self, obj: &glib::Object) {
        let self_ = obj.downcast_ref::<MainContentPage>().unwrap();

        self.account_button.set_popover(Some(&self.account_popover));
        self.menu_button.set_menu_model(Some(&self.primary_menu));

        self_.append(&self.headerbar);
        self_.append(&self.stack);
        self_.append(&self.switcher_bar);

        self_.show();
    }
}

impl WidgetImpl for MainContentPagePriv {}
impl BoxImpl for MainContentPagePriv {}

glib_wrapper! {
    pub struct MainContentPage(Object<glib::subclass::simple::InstanceStruct<MainContentPagePriv>,
        glib::subclass::simple::ClassStruct<MainContentPagePriv>,
        MainContentPageClass>) @extends gtk::Widget, gtk::Box;

    match fn {
        get_type => || MainContentPagePriv::get_type().to_glib(),
    }
}

impl MainContentPage {
    pub fn new(client: ClientRef) -> Self {
        let page = glib::Object::new(
            MainContentPage::static_type(),
            &[("orientation", &gtk::Orientation::Vertical)],
        )
        .unwrap()
        .downcast::<MainContentPage>()
        .unwrap();

        let self_ = MainContentPagePriv::from_instance(&page);
        self_.client.set(client.clone()).unwrap();

        let home = timelines::Home::new(client);
        self_.home_page.set_child(Some(&home.container));
        self_.home_timeline.set(home).unwrap();

        page
    }

    pub fn get_notifs_button(&self) -> &gtk::Button {
        let self_ = MainContentPagePriv::from_instance(self);
        &self_.notifs_button
    }

    pub fn show_menu(&self) {
        let self_ = MainContentPagePriv::from_instance(self);
        self_.menu_button.activate();
    }

    pub fn init(&self) {
        let self_ = MainContentPagePriv::from_instance(self);
        self_.home_timeline.get().unwrap().init();
    }
}
