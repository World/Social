// notifications_page.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::translate::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use libhandy::auto::traits::*;
use libhandy::HeaderBarExt;

use elefren::entities::notification::Notification;
use elefren::http_send::*;
use elefren::page::OwnedPage;
use elefren::prelude::*;

use std::cell::RefCell;
use std::rc::Rc;
use std::thread;
use std::time::Duration;

use once_cell::sync::OnceCell;

use crate::i18n::*;
use crate::utils::lazy_load;
use crate::widgets::NotificationWidget;
use crate::ClientRef;

const CONSTRUCTOR: fn((ClientRef, Notification)) -> gtk::Box =
    move |tuple| NotificationWidget::new(tuple.0, tuple.1).container.clone();

#[derive(Clone)]
enum NotificationMsg {
    Initialized(OwnedPage<Notification, HttpSender>),
}

#[derive(Clone)]
pub struct NotificationsPagePriv {
    headerbar: libhandy::HeaderBar,
    back_button: gtk::Button,
    scrolledwindow: gtk::ScrolledWindow,
    clamp: libhandy::Clamp,
    notifs_list: gtk::ListBox,
    notifs: Rc<RefCell<Option<OwnedPage<Notification, HttpSender>>>>,
    sender: OnceCell<glib::Sender<NotificationMsg>>,
    client: OnceCell<ClientRef>,
}

impl ObjectSubclass for NotificationsPagePriv {
    const NAME: &'static str = "SoclNotificationsPage";
    type ParentType = gtk::Box;
    type Instance = glib::subclass::simple::InstanceStruct<Self>;
    type Class = glib::subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        Self {
            headerbar: libhandy::HeaderBar::new(),
            back_button: gtk::Button::from_icon_name(Some("go-previous-symbolic")),
            scrolledwindow: gtk::ScrolledWindow::new(),
            clamp: libhandy::Clamp::new(),
            notifs_list: gtk::ListBox::new(),
            notifs: Rc::new(RefCell::new(None)),
            sender: OnceCell::new(),
            client: OnceCell::new(),
        }
    }
}

impl ObjectImpl for NotificationsPagePriv {
    fn constructed(&self, obj: &glib::Object) {
        let self_ = obj.downcast_ref::<NotificationsPage>().unwrap();

        self.back_button.set_action_name(Some("win.back"));

        self.headerbar.pack_start(&self.back_button);
        let title_widget = gtk::Label::new(Some(&i18n("Notifications")));
        title_widget.get_style_context().add_class("title");
        self.headerbar.set_title_widget(Some(&title_widget));
        self.headerbar.set_show_title_buttons(true);

        self.notifs_list
            .set_selection_mode(gtk::SelectionMode::None);
        self.notifs_list.set_valign(gtk::Align::Start);

        self.clamp.set_maximum_size(800);
        self.clamp.set_tightening_threshold(400);
        self.clamp.set_child(Some(&self.notifs_list));
        self.clamp.set_hexpand(true);
        self.clamp.set_vexpand(true);

        self.scrolledwindow
            .set_property_hscrollbar_policy(gtk::PolicyType::Never);
        self.scrolledwindow.set_child(Some(&self.clamp));
        self.scrolledwindow.show();

        self_.append(&self.headerbar);
        self_.append(&self.scrolledwindow);

        self_.show();
    }
}

impl WidgetImpl for NotificationsPagePriv {}
impl BoxImpl for NotificationsPagePriv {}

glib_wrapper! {
    pub struct NotificationsPage(Object<glib::subclass::simple::InstanceStruct<NotificationsPagePriv>,
        glib::subclass::simple::ClassStruct<NotificationsPagePriv>,
        NotificationsPageClass>) @extends gtk::Widget, gtk::Box;

    match fn {
        get_type => || NotificationsPagePriv::get_type().to_glib(),
    }
}

impl NotificationsPage {
    pub fn new(client: ClientRef) -> Self {
        let page = glib::Object::new(
            NotificationsPage::static_type(),
            &[("orientation", &gtk::Orientation::Vertical)],
        )
        .unwrap()
        .downcast::<NotificationsPage>()
        .unwrap();

        let self_ = NotificationsPagePriv::from_instance(&page);
        self_.client.set(client).unwrap();

        let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        self_.sender.set(sender).unwrap();

        let notifs = self_.notifs.clone();
        let client = self_.client.get().unwrap();
        receiver.attach(None, clone!(@weak self_.notifs_list as list, @weak client => @default-return glib::Continue(false), move |msg| match msg {
            NotificationMsg::Initialized(page) => {
                notifs.replace(Some(page.clone()));
                let items = page.initial_items;
                let page: Vec<(ClientRef, Notification)> =
                    items.iter().map(|n| (client.clone(), n.clone())).collect();
                lazy_load(page, &list, CONSTRUCTOR, move || {});
                glib::Continue(true)
            }
        }));

        page
    }

    pub fn init(&self) {
        let self_ = NotificationsPagePriv::from_instance(self);
        let client = self_.client.get().unwrap().clone();
        let sender = self_.sender.get().unwrap().clone();

        // Set up thread for initial setup;
        thread::spawn(move || loop {
            match client.read().unwrap().as_ref().unwrap().notifications() {
                Ok(n) => {
                    send!(sender, NotificationMsg::Initialized(n.to_owned()));
                    break;
                }
                Err(_) => {
                    thread::sleep(Duration::from_secs(10));
                }
            }
        });
    }
}
