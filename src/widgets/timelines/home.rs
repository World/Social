// home.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::prelude::*;
use libhandy::auto::traits::*;

use elefren::entities::status::Status;
use elefren::http_send::*;
use elefren::page::OwnedPage;
use elefren::prelude::*;

use std::cell::RefCell;
use std::rc::Rc;
use std::thread;
use std::time::Duration;

use crate::utils::lazy_load;
use crate::widgets::TootWidget;
use crate::ClientRef;

use super::TimelineMsg;

const CONSTRUCTOR: fn((ClientRef, Status)) -> gtk::Box =
    move |tuple: (ClientRef, Status)| TootWidget::new(tuple.0, tuple.1).container.clone();

#[derive(Clone, Debug)]
pub struct Home {
    pub container: gtk::Stack,
    clamp: libhandy::Clamp,
    listbox: gtk::ListBox,
    client: ClientRef,
    timeline: Rc<RefCell<Option<OwnedPage<Status, HttpSender>>>>,
    sender: glib::Sender<TimelineMsg>,
}

impl Home {
    pub fn new(client: ClientRef) -> Rc<Self> {
        let listbox = gtk::ListBox::new();
        listbox.set_selection_mode(gtk::SelectionMode::None);

        let spinner = gtk::Spinner::new();
        spinner.start();
        spinner.show();

        let clamp = libhandy::Clamp::new();
        clamp.set_tightening_threshold(400);
        clamp.set_maximum_size(800);
        clamp.set_child(Some(&listbox));

        let container = gtk::Stack::new();
        container.set_hhomogeneous(false);
        container.set_vhomogeneous(false);
        container.add_named(&clamp, "list");
        container.add_named(&spinner, "spinner");
        container.set_visible_child_name("spinner");
        container.set_transition_type(gtk::StackTransitionType::Crossfade);

        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        let widget = Home {
            container,
            clamp,
            listbox,
            client,
            timeline: Rc::new(RefCell::new(None)),
            sender: tx,
        };

        let client = widget.client.clone();
        let tl = widget.timeline.clone();
        rx.attach(None, clone!(@weak widget.container as stack, @weak widget.listbox as list => @default-return glib::Continue(false), move |msg| match msg {
            TimelineMsg::Received(timeline) => {
                tl.replace(Some(timeline.to_owned()));
                let items = timeline.initial_items;
                let page: Vec<(ClientRef, Status)> = items
                    .iter()
                    .map(|status| (client.clone(), status.clone()))
                    .collect();
                lazy_load(page, &list, CONSTRUCTOR, clone!(@weak stack => move || {
                    stack.set_visible_child_name("list");
                }));
                glib::Continue(true)
            }
        }));

        Rc::new(widget)
    }

    pub fn init(&self) {
        let client = self.client.clone();
        let tx = self.sender.clone();
        // Fetch inital timeline; Retry after 2 seconds on failure.
        thread::spawn(move || loop {
            match client.read().unwrap().as_ref().unwrap().get_home_timeline() {
                Ok(timeline) => {
                    send!(tx, TimelineMsg::Received(timeline.to_owned()));
                    break;
                }
                Err(e) => {
                    eprintln!("Failed to get timeline: {:#?}", e);
                    thread::sleep(Duration::from_secs(2));
                }
            }
        });
    }
}
