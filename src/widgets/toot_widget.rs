//
// toot_widget.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;

use elefren::entities::status::Status;
use elefren::status_builder::Visibility;

use html2pango::*;

use chrono::{offset::TimeZone, prelude::*};
use std::{cell::RefCell, rc::Rc};

use crate::i18n::*;
use crate::{app::SoclApplication, widgets, ClientRef};

#[derive(Clone, Debug)]
pub struct TootWidget {
    pub container: gtk::Box,
    toot_text_label: gtk::Label,
    toot_name_label: gtk::Label,
    toot_time_label: gtk::Label,
    cw_label: gtk::Label,
    text_revealer: gtk::Revealer,
    reply_button: gtk::Button,
    favourite_button: gtk::ToggleButton,
    favourites_label: gtk::Label,
    boost_button: gtk::ToggleButton,
    boosts_label: gtk::Label,
    cw_revealer_btn: gtk::ToggleButton,
    expand_cw_label: gtk::Label,
    status: Rc<RefCell<Option<Status>>>,
}

impl TootWidget {
    pub fn new(client: ClientRef, status: Status) -> Rc<Self> {
        let widget = Rc::new(Self::default());

        // Check if it's a boost before building
        let mut status = status;
        if let Some(boosted_status) = status.reblog {
            status = *boosted_status;
        }

        widget.status.replace(Some(status.clone()));
        widget.set_time();

        // Set up labels
        if !status.spoiler_text.is_empty() {
            widget.cw_label.set_text(&status.spoiler_text);
            widget.cw_label.show();
            widget.cw_revealer_btn.show();
            widget.cw_revealer_btn.set_active(false);
        }

        widget.cw_revealer_btn.connect_toggled(
            clone!(@weak widget.expand_cw_label as lbl => move |btn| {
                if btn.get_active() {
                    lbl.set_text(&i18n("Show Less"));
                } else {
                    lbl.set_text(&i18n("Show More…"));
                }
            }),
        );

        widget
            .toot_text_label
            .set_markup(&markup_html(&status.content).unwrap().trim());

        widget.toot_name_label.set_markup(&format!(
            "<b>{}</b>  <span alpha=\"50%\">@{}</span>",
            &status.account.display_name, &status.account.acct
        ));
        widget
            .favourites_label
            .set_text(&status.favourites_count.to_string());
        widget
            .boosts_label
            .set_text(&status.reblogs_count.to_string());

        // See whether the user can boost based on visibility
        let boost_icon = match status.visibility {
            Visibility::Private => Some("rotation-locked-symbolic"),
            Visibility::Direct => Some("mail-unread-symbolic"),
            _ => None,
        };

        if let Some(icon) = boost_icon {
            widget.boost_button.set_sensitive(false);
            widget.boost_button.set_icon_name(icon);
        }

        if status.in_reply_to_id.is_some() {
            widget.reply_button.set_icon_name("post-reply-symbolic");
        }

        // Toggles
        widget
            .favourite_button
            .set_active(status.favourited.unwrap_or(false));

        widget
            .boost_button
            .set_active(status.reblogged.unwrap_or(false));

        // Set up reply action
        widget
            .reply_button
            .set_action_target_value(Some(&status.id.to_variant()));

        let own = client.clone();
        let rc = widget.clone();
        widget
            .boost_button
            .connect_clicked(move |btn| callbacks::boost_button_toggled(btn, &rc, &own));
        let own = client.clone();
        let rc = widget.clone();
        widget
            .favourite_button
            .connect_clicked(move |btn| callbacks::fav_button_toggled(btn, &rc, &own));

        actions::create(&widget, client);

        widget
    }

    fn update_counts(&self) {
        let s = self.status.borrow();
        let status = s.as_ref().unwrap();

        if let Some(reblog) = &status.reblog {
            self.favourites_label
                .set_text(&reblog.favourites_count.to_string());
            self.boosts_label
                .set_text(&reblog.reblogs_count.to_string());
        } else {
            self.favourites_label
                .set_text(&status.favourites_count.to_string());
            self.boosts_label
                .set_text(&status.reblogs_count.to_string());
        }
    }

    fn set_time(&self) {
        let status_time_utc = self
            .status
            .borrow()
            .as_ref()
            .unwrap()
            .created_at
            .naive_utc();

        let local_time = Local::now();
        let status_timestamp = local_time
            .timezone()
            .from_utc_datetime(&status_time_utc)
            .timestamp();

        let seconds = local_time.timestamp() - status_timestamp;
        let days = seconds / (24 * 60 * 60);
        let years = days / 365;
        let hours = (seconds % (24 * 60 * 60)) / (60 * 60);
        let minutes = seconds / 60;

        let mut unit = 'y';
        let mut since = years;
        if years < 1 {
            since = days;
            unit = 'd';
            if days < 1 {
                since = hours;
                unit = 'h';
                if hours < 1 {
                    since = minutes;
                    unit = 'm';
                    if minutes < 1 {
                        since = seconds;
                        unit = 's';
                    }
                }
            }
        }

        self.toot_time_label.set_text(&format!("{}{}", since, unit));
    }
}

mod callbacks {
    use super::*;
    use elefren::MastodonClient;

    pub(crate) fn boost_button_toggled<T>(
        button: &T,
        // Will this create strong references to the widget?
        widget: &Rc<TootWidget>,
        client: &ClientRef,
    ) where
        T: ToggleButtonExt,
    {
        let status = widget.status.borrow();
        let client_reader = client.read().unwrap();

        let prev_boosted = status.as_ref().unwrap().reblogged.unwrap_or(false);

        let result = if !prev_boosted {
            client_reader
                .as_ref()
                .unwrap()
                .reblog(&status.as_ref().unwrap().id)
        } else {
            client_reader
                .as_ref()
                .unwrap()
                .unreblog(&status.as_ref().unwrap().id)
        };

        // Drop the borrow before we mutate
        drop(status);

        match result {
            Ok(new_status) => {
                widget.status.replace(Some(new_status));
                widget.update_counts();
            }
            Err(_) => {
                button.set_active(prev_boosted);
            }
        }
    }

    pub(crate) fn fav_button_toggled<T>(
        button: &T,
        // Will this create strong references to the widget?
        widget: &Rc<TootWidget>,
        client: &ClientRef,
    ) where
        T: ToggleButtonExt,
    {
        let status = widget.status.borrow();
        let client_reader = client.read().unwrap();

        let prev_faved = status.as_ref().unwrap().favourited.unwrap_or(false);

        let result = if !prev_faved {
            client_reader
                .as_ref()
                .unwrap()
                .favourite(&status.as_ref().unwrap().id)
        } else {
            client_reader
                .as_ref()
                .unwrap()
                .unfavourite(&status.as_ref().unwrap().id)
        };

        // Drop the borrow before we mutate
        drop(status);

        match result {
            Ok(new_status) => {
                widget.status.replace(Some(new_status));
                widget.update_counts();
            }
            Err(_) => {
                button.set_active(prev_faved);
            }
        }
    }
}

mod actions {
    use super::*;
    use gio::ActionMapExt;

    pub fn create(toot_widget: &TootWidget, client: ClientRef) {
        let group = gio::SimpleActionGroup::new();
        let app = SoclApplication::get_default().unwrap();
        let window = app
            .get_active_window()
            .unwrap()
            .downcast::<libhandy::ApplicationWindow>()
            .unwrap();

        let variant = glib::VariantTy::new("s").unwrap();
        action!(
            group,
            "reply",
            Some(variant),
            clone!(@weak window, @weak client => move |_, variant | {
                let variant = variant.unwrap();
                let id = variant.get::<String>().unwrap();
                let post_builder = widgets::PostBuilder::new(&window, client, Some(id));
                post_builder.present();
            })
        );

        toot_widget
            .container
            .insert_action_group("post", Some(&group));
    }
}

impl Default for TootWidget {
    fn default() -> Self {
        let builder = gtk::Builder::from_resource("/org/gnome/Social/gtk/toot_widget.ui");

        let container = builder.get_object("toot_widget").unwrap();
        let toot_text_label = builder.get_object("toot_text_label").unwrap();
        let toot_name_label = builder.get_object("toot_name_label").unwrap();
        let toot_time_label = builder.get_object("toot_time_label").unwrap();
        let cw_label = builder.get_object("cw_label").unwrap();
        let text_revealer = builder.get_object("toot_text_revealer").unwrap();
        let reply_button = builder.get_object("reply_button").unwrap();
        let favourite_button = builder.get_object("favourite_button").unwrap();
        let favourites_label = builder.get_object("favourites_label").unwrap();
        let boost_button = builder.get_object("boost_button").unwrap();
        let boosts_label = builder.get_object("boosts_label").unwrap();
        let cw_revealer_btn = builder.get_object("cw_revealer_btn").unwrap();
        let expand_cw_label = builder.get_object::<gtk::Label>("expand_cw_label").unwrap();

        expand_cw_label.set_text(&i18n("Show More…"));
        expand_cw_label
            .get_style_context()
            .add_class("expand-button");

        Self {
            container,
            toot_text_label,
            toot_name_label,
            toot_time_label,
            cw_label,
            text_revealer,
            reply_button,
            favourite_button,
            favourites_label,
            boost_button,
            boosts_label,
            cw_revealer_btn,
            expand_cw_label,
            status: Rc::new(RefCell::new(None)),
        }
    }
}
