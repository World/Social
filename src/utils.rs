//
// utils.rs
//
// Copyright 2018 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::object::*;
use gtk::{prelude::*, Widget};

use anyhow::Result;

use std::{
    fs::File,
    io::{Read, Write},
    path::Path,
    process::Command,
};

use elefren::{http_send::HttpSender, registration::Registered, scopes::Scopes, Registration};

use crate::config;

#[macro_export]
macro_rules! media {
    ($base: expr, $url: expr, $dest: expr) => {
        dw_media($base, $url, false, $dest, 0, 0)
    };
    ($base: expr, $url: expr) => {
        dw_media($base, $url, false, None, 0, 0)
    };
}

pub fn get_media(url: &str) -> Result<Vec<u8>> {
    let client = reqwest::Client::new();
    let conn = client.get(url);
    let mut res = conn.send()?;

    let mut buffer = Vec::new();
    res.read_to_end(&mut buffer)?;

    Ok(buffer)
}

#[allow(dead_code)]
pub fn download_file(url: &str, fname: String, dest: Option<&str>) -> Result<String> {
    let pathname = fname.clone();
    let p = Path::new(&pathname);
    if p.is_file() {
        if dest.is_none() {
            return Ok(fname);
        }

        let moddate = p.metadata()?.modified()?;
        // one minute cached
        if moddate.elapsed()?.as_secs() < 60 {
            return Ok(fname);
        }
    }

    let mut file = File::create(&fname)?;
    let buffer = get_media(url)?;
    file.write_all(&buffer)?;

    Ok(fname)
}

pub fn open_browser(url: &str) -> Result<()> {
    Command::new("xdg-open").arg(url).output()?;
    Ok(())
}

pub fn authorize_url(base_url: &str) -> Result<Registered<HttpSender>> {
    let maybe_reg = Registration::new(base_url)
        .client_name("Social")
        .scopes(Scopes::all())
        .website("https://gitlab.gnome.org/World/Social")
        .redirect_uris(format!("oauth2redirect://{}/", config::APP_ID))
        .build();
    anyhow::ensure!(maybe_reg.is_ok(), "Failed to build registration info");
    let regist = maybe_reg.unwrap();

    let maybe_url = regist.authorize_url();
    anyhow::ensure!(maybe_url.is_ok(), "Failed to authorize URL");
    let url = maybe_url.unwrap();

    open_browser(&url)?;

    Ok(regist)
}

pub fn error_dialog<T: IsA<gtk::Window>>(msg: &str, parent: &T) {
    let dialog = gtk::MessageDialog::new(
        Some(parent),
        gtk::DialogFlags::empty(),
        gtk::MessageType::Error,
        gtk::ButtonsType::Close,
        &msg,
    );

    dialog.set_modal(true);

    dialog.connect_response(move |dialog, _| {
        // The only response possible is Close
        dialog.close();
    });

    dialog.present();
}

// From Podcasts: podcasts-gtk/src/utils.rs

/// Lazy evaluates and loads widgets to the parent `container` widget.
///
/// Accepts an `IntoIterator`, `data`, as the source from which each widget
/// will be constructed. An `FnMut` function that returns the desired
/// widget should be passed as the widget `constructor`. You can also specify
/// a `callback` that will be executed when the iteration finish.
///
/// ```no_run
/// # struct Message;
/// # struct MessageWidget(gtk::Label);
///
/// # impl MessageWidget {
/// #    fn new(_: Message) -> Self {
/// #        MessageWidget(gtk::Label::new("A message"))
/// #    }
/// # }
///
/// let messages: Vec<Message> = Vec::new();
/// let list = gtk::ListBox::new();
/// let constructor = |m| MessageWidget::new(m).0;
/// lazy_load(messages, list, constructor, || {});
/// ```
///
/// If you have already constructed the widgets and only want to
/// load them to the parent you can pass a closure that returns it's
/// own argument to the constructor.
///
/// ```no_run
/// # use std::collections::binary_heap::BinaryHeap;
/// let widgets: BinaryHeap<gtk::Button> = BinaryHeap::new();
/// let list = gtk::ListBox::new();
/// lazy_load(widgets, list, |w| w, || {});
/// ```
pub(crate) fn lazy_load<T, F, W, U>(
    data: T,
    container: &gtk::ListBox,
    mut contructor: F,
    callback: U,
) where
    T: IntoIterator + 'static,
    T::Item: 'static,
    F: FnMut(T::Item) -> W + 'static,
    W: IsA<Widget> + WidgetExt,
    U: Fn() + 'static,
{
    let func = clone!(@weak container => move |x| {
        let widget = contructor(x);
        container.insert(&widget, -1);
        widget.show();
    });
    lazy_load_full(data, func, callback);
}

/// Iterate over `data` and execute `func` using a `gtk::idle_add()`,
/// when the iteration finishes, it executes `finish_callback`.
///
/// This is a more flexible version of `lazy_load` with less constrains.
/// If you just want to lazy add `widgets` to a `container` check if
/// `lazy_load` fits your needs first.
#[cfg_attr(feature = "cargo-clippy", allow(clippy::redundant_closure))]
pub(crate) fn lazy_load_full<T, F, U>(data: T, mut func: F, finish_callback: U)
where
    T: IntoIterator + 'static,
    T::Item: 'static,
    F: FnMut(T::Item) + 'static,
    U: Fn() + 'static,
{
    let mut data = data.into_iter();
    glib::idle_add_local(move || {
        data.next()
            .map(|x| func(x))
            .map(|_| glib::Continue(true))
            .unwrap_or_else(|| {
                finish_callback();
                glib::Continue(false)
            })
    });
}
