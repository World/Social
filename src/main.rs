/* Social: A Mastodon client
 * Copyright (C) 2017-2018 Christopher Davis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate gdk4 as gdk;
extern crate gtk4 as gtk;
extern crate libhandy4 as libhandy;
extern crate serde_derive;

#[macro_use]
extern crate gtk_macros;

#[macro_use]
extern crate glib;

#[macro_use]
extern crate log;

#[macro_use]
mod utils;

mod app;
mod config;
mod i18n;
mod passwords;
mod widgets;
mod window;

pub use crate::window::ClientRef;

use crate::app::SoclApplication;

fn main() {
    gtk::init().expect("Failed to initialize GTK");
    pretty_env_logger::init();

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/org.gnome.Social.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    SoclApplication::run();
}
